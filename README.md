# Structurizr Demo

## Intent

Demonstrate an example project using Structurizr DSL and Structurizr Lite

## More about Structurizer

Confluence: LINK

## Getting Started

* Follow this guide: <https://dev.to/simonbrown/getting-started-with-structurizr-lite-27d0>
  * This will help you get Structurizr Lite running on your local machine
* Clone this repo to your local system
* Edit the start.sh file to reflect the correct path for your system
* Run `chmod +x start.sh`
* Run `./start`

## Links

* <https://structurizr.com/>
* <https://structurizr.com/help>
* <https://structurizr.com/help/quick-navigation>
* <https://structurizr.com/help/diagram-navigation>

## VS Code Extensions

* C4 DSL Extension <https://marketplace.visualstudio.com/items?itemName=systemticks.c4-dsl-extension>
* Structurizr <https://marketplace.visualstudio.com/items?itemName=ciarant.vscode-structurizr>
